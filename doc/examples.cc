//
// $Id: examples.cc,v 1.11 2011-01-24 09:39:38 cholm Exp $
//
//   General C++ parser and lexer
//   Copyright (C) 2002  Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   doc/examples.cc
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page examples Examples 
    Here are the various examples.  All examples that end on 
    @c -test.cc are test suits used in the development mainly.
    Examples that end in @c -exa.cc are examples extracted from the
    GSL reference manual, and converted to GSLMM. 
 */

/** @example annealing/annealing-test.cc 
    @par Test of annealing classes. */
/** @example blas/blas-test.cc 
    @par Test of blas classes. */
/** @example chebyshev/chebyshev-test.cc 
    @par Test of chebyshev classes. */
/** @example combination/combination-test.cc 
    @par Test of combination classes. */
/** @example compleks/complex-test.cc 
    @par Test of complex classes. */
/** @example constant/constant-test.cc 
    @par Test of constant classes. */
/** @example differentiation/differentiation-test.cc 
    @par Test of differentiation classes. */
/** @example eigen/eigen-test.cc 
    @par Test of eigen classes. */
/** @example error/error-test.cc 
    @par Test of error handling classes. */
/** @example fit/fit-test.cc 
    @par Test of fitting classes. */
/** @example fit/linear-ex.cc 
    @par Example of using linear least-square fitting. */
/** @example fit/fit-ex.cc 
    @par Example of using general fitting classes. */
/** @example fit/fit-gaussian-ex.cc 
    @par Example of using general fitting classes to fit a Guassian. */
/** @example fourier/fourier-test.cc 
    @par Test of fourier classes. */
/*  @example function/function-test.cc 
    @par Test of function classes. */
/** @example hankel/hankel-test.cc 
    @par Test of hankel classes. */
/** @example histogram/histogram-test.cc 
    @par Test of histogram classes. */
/** @example histogram/histogram-ex.cc 
    @par Example of histogram classes. */
/** @example ieee/ieee-test.cc 
    @par Test of ieee classes. */
/** @example integration/integration-test.cc 
    @par Test of integration classes. */
/** @example interpolation/interpolation-test.cc 
    @par Test of interpolation classes. */
/** @example interpolation/spline-ex.cc 
    @par Example of spline classes. */
/** @example linear/linear-test.cc 
    @par Test of linear classes. */
/** @example math/math-test.cc 
    @par Test of math classes. */
/** @example minimization/minimization-test.cc 
    @par Test of minimization classes. */
/** @example montecarlo/montecarlo-test.cc 
    @par Test of montecarlo classes. */
/** @example ntuple/ntuple-test.cc 
    @par Test of tuples classes. */
/** @example ntuple/ntuple-ex.cc 
    @par Example of tuple classes. */
/** @example ordinary/ordinary-test.cc 
    @par Test of ordinary classes. */
/** @example permutation/permutation-test.cc 
    @par Test of permuation classes. */
/** @example polynomial/polynomial-test.cc 
    @par Test of polynomial classes. */
/** @example random/generator-test.cc 
    @par Test of random number generator classes. */
/** @example random/distribution-test.cc 
    @par Test of random distribution classes. */
/** @example random/quasi_generator-test.cc 
    @par Test of quasi random number generator classes. */
/** @example root/root-test.cc 
    @par Test of root classes. */
/** @example series/series-test.cc 
    @par Test of series classes. */
/** @example sort/sort-test.cc 
    @par Test of sort classes. */
/** @example special/special-test.cc 
    @par Test of special classes. */
/** @example statistic/statistic-test.cc 
    @par Test of statistic classes. */
/** @example vectormatrix/matrix-test.cc 
    @par Test of matrix classes. */
/** @example vectormatrix/vector-test.cc 
    @par Test of vector classes. */

//
// EOF
//
