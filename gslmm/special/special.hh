//
// $Id: special.hh,v 1.2 2003-03-12 13:52:06 cholm Exp $ 
//  
//  gslmm::special
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_special
#define GSLMM_special

/** @file   special/special.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  special class */

/** @defgroup special Special Functions
    @todo Implement these classes. */

#endif
//____________________________________________________________________
//
// EOF
//
