//
// $Id: histogram.hh,v 1.3 2003-03-15 18:57:01 cholm Exp $ 
//  
//  gslmm::histogram
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_histogram
#define GSLMM_histogram

/** @file   histogram/histogram.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  histogram class */

/** @defgroup histogram Histograms. */

#ifndef GSLMM_histogram_1d
#include <gslmm/histogram/histogram_1d.hh>
#endif
#ifndef GSLMM_histogram_2d
#include <gslmm/histogram/histogram_2d.hh>
#endif
  
#endif
//____________________________________________________________________
//
// EOF
//
