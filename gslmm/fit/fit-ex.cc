#include <gslmm/fit/fit.hh>
#include <iostream>
#include <sstream>
#include <gslmm/random/gaussian.hh>
#include <gslmm/error/error_handler.hh>
#include <iomanip>
#include <vector>

/**
 * Fit function class to fit an exponential spectra.
 * 
 */
struct expo 
{
  /** The data */
  std::vector<double> y;
  /** The errors */
  std::vector<double> sigma;
  
  int function(const gslmm::vector<double>& x, gslmm::vector<double>& f)
  {
    double A      = x[0];
    double lambda = x[1];
    double b      = x[2];
    for (size_t i = 0; i < y.size(); i++) {
      double t  = i;
      double yi = A * exp(-lambda * t) + b;
      f[i]      = (yi - y[i]) / sigma[i];
    }
    return GSL_SUCCESS;
  }
  int jacobian(const gslmm::vector<double>& x, gslmm::matrix<double>& j)
  {
    double A      = x[0];
    double lambda = x[1];
    double b      = x[2];
    for (size_t i = 0; i < y.size(); i++) {
      double t   = i;
      double s   = sigma[i];
      double e   = exp(-lambda * t);
      j(i, 0) = e / s;
      j(i, 1) = -t * A * e / s;
      j(i, 2) = 1 / s;
    }
    return GSL_SUCCESS;
  }
  int iterate() { return GSL_SUCCESS; }
  expo(size_t n) 
    : y(n), sigma(n)
  {
    gslmm::generator::env_setup();
    gslmm::generator g;
    gslmm::gaussian  r(0.1, g);
    
    for (size_t i = 0; i < n; i++) {
      double t = i;
      y[i]     = 1. + 5 * exp(-0.1 * t) + r.sample();
      sigma[i] = .1;
    }
  }
  void print_graph(std::ostream& o, double A, double lambda, double b) const
  {
    size_t n = y.size();
    o << "#m=6,S=0" << std::endl;
    for (size_t i = 0; i < n * 10; i++) {
      double x = i / 10.;
      double y = A * exp(-lambda * x) + b;
      o << x << " " << y << " " << 0 << std::endl;
    }

    o << "#m=-3,S=2" << std::endl;
    for (size_t i = 0; i < n; i++) { 
      o << i << " " << y[i] << " " << sigma[i] << std::endl;
    }
  }
};

void 
print_status(size_t iter, const gslmm::fitter<expo>& fit, bool cont) 
{
  gslmm::vector<double> p(fit.position());
  gslmm::vector<double> f(fit.values());
  std::cerr << std::setw(4) << iter << " x=("
	    << std::setw(10) << p[0] << ","
	    << std::setw(10) << p[1] << ","
	    << std::setw(10) << p[2] << ") |f(x)|="
	    << std::setw(10) << f.length() << "\t" 
	    << (cont ? "continue" : "done") << std::endl;
}

/**
 * Test function.  The status and result of the fit is displayed on
 * standard error, while the data and the fit is shown on standard
 * output.  To see the result of the fit and a plot of the data
 * together with the fitted function, do 
 *
 * @verbatim 
 *    ./fit-ex | graph -T X -I e -C -l y
 * @endverbatim
 *
 * To see only the result of the fit, do
 *
 * @verbatium 
 *   ./fit-ex > /dev/null
 * @endverbatim
 */

int main()
{
  expo e(40);
  gslmm::vector<double> initial(3);
  initial[0] = 1;
  gslmm::fitter<expo> fit;
  fit.initialize(e, gslmm::fitter<expo>::scaled_levenberg_marquardt, 
	       initial, 40);
  
  print_status(0, fit, true);
  size_t iter = 1;
  int status  = 0;
  bool cont = true;
  do {
    status = fit.iterate();
    if (status) { 
      std::cerr << "bad status " << status << std::endl;
      break;
    }
    cont = !fit.test_delta(1e-4, 1e-4);
    print_status(iter, fit, cont);
  } while (cont && ++iter < 500);
  gslmm::matrix<double> c(3,3);
  gslmm::vector<double> p(fit.position());
  fit.covariance(c, 0);
  
  for (size_t i = 0; i < 3; i++) {
    std::cerr << (i == 0 ? "A" : (i == 1 ? "lambda" : "b"))
	      << "\t\t= " << std::setw(10) << p[i] 
	      << " +/" << std::setw(10) << sqrt(c(i,i)) << std::endl;
  }
  gslmm::vector<double> f(fit.values());
  double chisq = f.length();
  std::cerr << "chi^2/dof\t= " << std::setw(10) << (chisq*chisq) 
	    << " / 37 = " << (chisq*chisq)/37 << "\nstatus=" 
	    << gslmm::error_handler<>::code_to_string(status) << std::endl;

  e.print_graph(std::cout, p[0], p[1], p[2]);

  return status;
}

  

  
      
