//
// $Id: complex.hh,v 1.2 2003-03-01 19:45:21 cholm Exp $ 
//  
//  gslmm::complex
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSL_complex
#define GSL_complex

/** @file   complex.hh
    @author Christian Holm
    @date   Sat Mar 01 19:07:27 2003
    @brief  Include all declarations of the complex classes */

/** @defgroup complex Complex numbers */

#ifndef GSLMM_complex_double
# include <gslmm/compleks/complex_double.hh>
#endif

#endif
//____________________________________________________________________
//
// EOF
//
