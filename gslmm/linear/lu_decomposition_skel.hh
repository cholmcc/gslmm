//
// $Id: lu_decomposition_skel.hh,v 1.6 2006-05-09 07:40:34 cholm Exp $ 
//  
// WARNING: automatically generated by @progname@
//          options: @skel@, @type@.  
//          Do not edit this file.
//
//  gslmm::linear@name@
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_linear_lu_decomposition@name@
#define GSLMM_linear_lu_decomposition@name@
#line 29 "lu_decomposition_skel.hh"
#ifndef __GSL_LINALG_H__
# include <gsl/gsl_linalg.h>
#endif
#ifndef GSLMM_permutation_permutation
# include <gslmm/permutation/permutation.hh>
#endif
#ifndef GSLMM_linear_linear_base
# include <gslmm/linear/linear_base.hh>
#endif
#ifndef GSLMM_matrixvector_matrix@name@
# include <gslmm/vectormatrix/matrix@name@.hh>
#endif
#ifndef GSLMM_matrixvector_vector@name@
# include <gslmm/vectormatrix/vector@name@.hh>
#endif
#if @complex@
# ifndef GSLMM@name@
#  include <gslmm/compleks/@cname@.hh>
# endif
#endif

/** @file   linear/lu_decomposition@name@.hh
    @author Christian Holm
    @date   Mon Mar 10 14:50:12 2003
    @brief  Base template for Linear Algebra classes */

namespace gslmm
{
  /** @brief class template specialisation for LU
      decompositions of matricies of type @type@.  
      @ingroup lu_decomposition_group

      This functions factorize the square matrix @f$ A@f$ into the LU 
      decomposition @f$ PA = LU@f$.  

      The algorithm used in the decomposition is Gaussian Elimination
      with partial pivoting (Golub & Van Loan, `Matrix Computations',
      Algorithm 3.4.1).
   */
  template <>
  struct lu_decomposition<@type@> : public matrix<@type@>
  {
    /** The vector type */
    typedef vector<@type@> vector_type;
    /** The matrix type */
    typedef matrix<@type@> matrix_type;
    /** The element type */
    typedef matrix_type::value_type value_type;
    /** The element type */
    typedef matrix_type::element_type element_type;
  private:
    /** The low level matrix type */
    typedef matrix_type::data_type data_type;
    /** The LU decomposition of the input matrix */
    // matrix_type  _lu;
    /** Reference to the input matrix */
    const matrix_type& _original;
    /** The permutation vector */
    permutation _permutation;
    /** The sign of the permutation */
    int         _sign;
  public:
    /** Create an LU decomposition of the input matrix @a m. 
	A reference to the input matrix is stored for later use. */
    lu_decomposition(const matrix_type& m) 
      : matrix_type(m), 
	_original(m), 
	_permutation(m.row_size()),
	_sign(0)
    {
      int ret = gsl_linalg@low@_LU_decomp(_matrix, 
					  _permutation._permutation,
					  &_sign);
      (void)ret;
    }
    /** This member function solve the system @f$ A x = b@f$ using
	the LU decomposition of @f$ A@f$. 
	@param b The vector @f$ b@f$
	@param x The vector @f$ x@f$
	@return true on success  */
    bool solve(const vector_type& b, vector_type& x) const
    {
      int ret = gsl_linalg@low@_LU_solve(_matrix, 
					 _permutation._permutation,
					 b._vector, x._vector);
      return (ret == 0);
    }
    /** This member function solve the system @f$ A x = b@f$ using
	the LU decomposition of @f$ A@f$. 
	@param x On input, the vector @f$ b@f$, and on output the
	vector @f$ x@f$  */
    bool solve(vector_type& x) const
    {
      int ret = gsl_linalg@low@_LU_svx(_matrix, 
				       _permutation._permutation,
				       x._vector);
      return (ret == 0);
    }
   /** This member function apply an iterative improvement to @f$
       x@f$, the solution of @f$ A x = b@f$, using the LU
       decomposition of @f$ A@f$. The initial residual @f$ r = A x - b
       @f$ is also computed and returned.
       @param b The input vector @f$ b@f$ 
       @param x The output solution vector @f$ x@f$ 
       @param r The initial residual vector @f$ r@f$ 
       @return True on success. */
   bool refine(const vector_type& b, vector_type& x, vector_type& r) const
    {
      int ret = gsl_linalg@low@_LU_refine(_original._matrix, 
					  _matrix, 
					  _permutation._permutation,
					  b._vector, x._vector, r._vector);
      return (ret == 0);
    }
    /** This member function compute the inverse of a matrix @f$ A@f$
	from its LU decomposition, storing the result in the matrix
	@a inv.  The inverse is computed by solving the system @f$ A x
	= b @f$ for each column of the identity matrix.  It is
	preferable to avoid direct computation of the inverse whenever
	possible. 
	@param inv The inverse of the matrix @f$ A@f$  
	@return True on success.  */
    bool inverse(matrix_type& inv) const
    {
      int ret = gsl_linalg@low@_LU_invert(_matrix, _permutation._permutation, 
					  inv._matrix);
      return (ret == 0);
    }

  /** This member function compute the determinant of a matrix @f$
      A@f$ from its LU decomposition. The determinant is computed as
      the product of the diagonal elements of @f$ U@f$ and the sign of
      the row permutation..
      @return The determinant of @f$ A@f$ */
    value_type determinant() const
    {
      value_type ret = 
	gsl_linalg@low@_LU_det(const_cast<data_type*>(_matrix), _sign);
      return ret;
    }
    /** This member function compute the logarithm of the absolute
	value of the determinant of a matrix @f$ A@f$,
	@f$\ln|det(A)|@f$, from its LU decomposition.  This function
	may be useful if the direct computation of the determinant
	would overflow or underflow. 
	@return The logarithm of the absolute value of the @f$
	det(A)@f$ */ 
    element_type log_determinant() const
    {
      element_type ret = 
	gsl_linalg@low@_LU_lndet(const_cast<data_type*>(_matrix));
      return ret;
    }
    /** This member function compute the sign or phase factor of the 
	determinant of a matrix @f$ A@f$, @f$ det(A)/|det(A)|@f$, from
	its LU decomposition
	@return sign of the determinant of @f$ A@f$ */
    value_type determinant_sign() 
    {
      value_type ret = gsl_linalg@low@_LU_sgndet(_matrix, _sign);
      return ret;
    }
    /** @return Sign of the permutation matrix @f$ P@f$ */
    int sign() const { return _sign; }
    /** @return The matrix @f$ L@f$ of the LU decomposition of @f$
	A@f$ */ 
    matrix_type l() const 
    {
      matrix_type ret(*this);
      for (size_t row = 0; row < row_size(); row++) {
	for (size_t col = row+1; col < column_size(); col++) {
	  ret(row, col) = 0;
	}
      }
      return ret;
    }
    /** @return The matrix @f$ U@f$ of the LU decomposition of @f$
	A@f$ */ 
    matrix_type u() const 
    {
      matrix_type ret(*this);
      for (size_t row = 0; row < row_size(); row++) 
	for (size_t col = 0; col < row; col++) 
	  ret(row, col) = 0;
      return ret;
    }
    /** @return The permutation matrix @f$ P@f$ of the LU
	decomposition of @f$ A@f$ */ 
    matrix_type p() const 
    { 
      matrix_type ret(row_size(), column_size(), true);
      for (size_t col = 0; col < column_size(); col++) {
	size_t irow = _permutation[col];
	ret(irow, col) = 1;
      }
      return ret;
    } 
    /** @return The matrix @f$ LU@f$ of the LU decomposition of @f$
	A@f$ */  
    const matrix_type& lu() const 
    {
      return *this;
    }
  };
}

#if @double@ && @complex@
namespace gslmm
{
  //__________________________________________________________________
  /** right / operation between matricies (complex<double>)
      For complex matrices,
      @f$ A / B = AB^{-1} = \left({B^{H}}^{-1}A^H\right)^T@f$. 
      In any case, the division can be computed by doing an LU
      decomposition of @f$ B^H@f$ and perform a back-substitution of the
      columns of @f$ A^H@f$, and computing the transpose hermitian
      conjugate to the resulting matrix.  Note, that the LU
      decomposition requires that the matrix @f$ B@f$ is square. 
      @ingroup linear
      @param lhs The left hand operand @f$ A @f$
      @param rhs The right hand operand @f$ B @f$
      @return new matrix, @f$ m_{ij} = A_{ij} B^{-1}_{ij} @f$ */
  template <>
  inline matrix<complex<double> >
  operator/(const matrix<complex<double> >& lhs, 
	    const matrix<complex<double> >& rhs)
  {    
    matrix<complex<double> > rt(rhs.column_size(),rhs.column_size());
    matrix<complex<double> > lhst(lhs,true,true);
    matrix<complex<double> > rhst(rhs,true,true);
    lu_decomposition<complex<double> > decomp(rt);
    int s = 0;
    for (size_t c = 0; c < lhst.column_size(); c++) {
      vector<complex<double> > v = lhst.column(c);
      if (!decomp.solve(v)) s++;
      for (size_t j = 0; j < v.size(); j++) rt(j, c) = v[j];
    }
    if (s != 0) std::cerr << "Error computing AB^{-1}" << std::endl;
    matrix<complex<double> > r(rt,true,true);
    return r;
  }
}
#else
#ifndef GSLMM_linear_qr_decomposition@name@
# include <gslmm/linear/qr_decomposition@name@.hh>
#endif
#ifndef GSLMM_linear_lq_decomposition@name@
# include <gslmm/linear/lq_decomposition@name@.hh>
#endif
namespace gslmm
{
  //__________________________________________________________________
  /** right / operation between matricies (double)
      Note that for real matrices,
      @f$ A / B = AB^{-1} = \left({B^{T}}^{-1}A^T\right)^T@f$ 
      In any case, the division can be computed by doing an
      decomposition of @f$ B^T@f$ or and perform a back-substitution
      of the columns of @f$ A^T@f$, and computing the transpose
      the resulting matrix.   If the matrix @f$ B@f$ is square, then
      an LU decomposition is used.  If it has more columns than rows,
      then a QR decomposition is used.   Finally, if @f$ B@f$ has more
      rows than columns, a LQ decomposition is used. 
      Note, that the two matrices @f$ A, B@f$ must have the same number
      of columns.  The returned matrix is square with dimension of the
      input matrices column size. 
      @ingroup linear
      @param lhs The left hand operand @f$ A @f$
      @param rhs The right hand operand @f$ B @f$
      @return new matrix, @f$ m_{ij} = A_{ij} B^{-1}_{ij} @f$ */
  template <>
  inline matrix<double>
  operator/(const matrix<double>& lhs, const matrix<double>& rhs)
  {    
    size_t m = rhs.row_size();
    size_t n = rhs.column_size();
    matrix<double> rt(m,m);
    matrix<double> lhst(lhs,true);
    matrix<double> rhst(rhs,true);
    int s = 0;
    if (n == m) {
      lu_decomposition<double> decomp(rhst);
      for (size_t c = 0; c < lhst.column_size(); c++) {
	vector<double> v = lhst.column(c);
	if (!decomp.solve(v)) s++;
	for (size_t j = 0; j < v.size(); j++) rt(j, c) = v[j];
      }
    }
    else if (rhst.row_size() > rhst.column_size()) {
      qr_decomposition<double> decomp(rhst);
      for (size_t c = 0; c < lhst.column_size(); c++) {
	vector<double> v = lhst.column(c);
	vector<double> x(rhst.column_size());
	vector<double> res(v.size());
	if (!decomp.solve(v,x,res)) s++;
	for (size_t j = 0; j < x.size(); j++) rt(j, c) = x[j];
      }
    }
    else {
      std::cout << "Case where row size < column size not handled yet" 
		<< std::endl;
      return lhs;
    }
    matrix<double> r(rt,true);
    if (s != 0) GSL_ERROR_VAL("Error computing AB^{-1}",GSL_FAILURE,r);
    return r;
  }
}
#endif

#endif
//____________________________________________________________________
//
// EOF
//
  
