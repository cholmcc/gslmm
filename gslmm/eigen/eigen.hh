//
// $Id: eigen.hh,v 1.4 2011-01-24 09:57:30 cholm Exp $ 
//  
//  gslmm::eigen
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_eigen
#define GSLMM_eigen

/** @file   eigen/eigen.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  eigen class */

/** 
 * @defgroup eigen Eigensystems
 * 
 * This module contains classes for computing eigenvalues and
 * eigenvectors of matrices.  There are routines for real symmetric,
 * real nonsymmetric, complex hermitian, real generalized
 * symmetric-definite, complex generalized hermitian-definite, and
 * real generalized nonsymmetric eigensystems. Eigenvalues can be
 * computed with or without eigenvectors.  The hermitian and real
 * symmetric matrix algorithms are symmetric bidiagonalization
 * followed by QR reduction. The nonsymmetric algorithm is the Francis
 * QR double-shift.  The generalized nonsymmetric algorithm is the QZ
 * method due to Moler and Stewart.
 *
 * @todo Implement these classes. 
 */
namespace gslmm
{
  template <typename T> struct matrix;
  template <typename T> struct matrix_manip;
  template <typename T> struct vector;

  template <typename T>
  struct eigen 
  {
    enum sort_method {
      ascending,
      descending, 
      absolute_ascending, 
      absolute_descending
    };
    
    eigen(matrix<T>& m);
    eigen(matrix_manip<T>& m);
    vector<T>& values();
    matrix<T>& vectors();
    void sort(sort_method m);
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
