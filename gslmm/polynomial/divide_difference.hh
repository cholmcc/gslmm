//
// $Id: divide_difference.hh,v 1.2 2003-03-01 19:46:02 cholm Exp $ 
//  
//  gslmm::divide_difference
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_divide_difference
#define GSLMM_divide_difference

/** @file   divide_difference.hh
    @author Christian Holm
    @date   Sat Mar 01 12:56:22 2003
    @brief  Newtons divide difference representation of a polynomial */

#ifndef GSLMM_divide_difference_double
# include <gslmm/polynomial/divide_difference_double.hh>
#endif 

#endif
//____________________________________________________________________
//
// EOF
//
