//
// $Id: taylor_expansion.hh,v 1.1 2003-03-10 12:39:48 cholm Exp $ 
//  
//  gslmm::taylor_expansion
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_taylor_expansion
#define GSLMM_taylor_expansion

/** @file   taylor_expansion.hh
    @author Christian Holm
    @date   Sat Mar 08 18:31:32 2003
    @brief  Representation of a tayler expansion */

#ifndef GSLMM_taylor_expansion_double
# include <gslmm/polynomial/taylor_expansion_double.hh>
#endif

#endif
//____________________________________________________________________
//
// EOF
//
