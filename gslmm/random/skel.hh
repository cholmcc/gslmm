//
// $Id: skel.hh,v 1.3 2006-04-26 23:08:16 cholm Exp $ 
//  
//  gslmm::@name@
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_random_@name@
#define GSLMM_random_@name@

/** @file   random/@name@.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  random @name@ classes */

#ifndef GSLMM_random_distribution
# include <gslmm/random/distribution.hh>
#endif

namespace gslmm 
{
  /** @class @name@ random/@name@.hh <gslmm/random/@name@.hh> 
      @brief @Name@ distribution interface class. 
      @ingroup dists 

      The @Name@ PDF is 
      @f[
      @f]

   */
  class @name@ : public distribution<double>
  {
  protected:
    /** The base type */
    typedef distribution<double> base_type;
  public:
    /** Constructor
	@param r The random number generator */
    @name@(generator& r) 
      : base_type(r)
    {}
    /** Destructor  */
    virtual ~@name@() {}
    
    /** Sample the distribution. 
	@return The sample. */
    virtual return_type sample() 
    {
      return gsl_ran_@name@(rng());
    }
    
    /** @see gslmm::distribution<Return,Arg>::pdf */
    virtual double pdf(const return_type& x) 
    {
      return gsl_ran_@name@_pdf(x);
    }
    
    /** @see gslmm::distribution<Return,Arg>::lower_cdf */
    virtual double lower_cdf(const return_type& x) 
    {
      return gsl_cdf_@name@_P(x);
    }
    /** @see gslmm::distribution<Return,Arg>::upper_cdf */
    virtual double upper_cdf(const return_type& x)
    {
      return gsl_cdf_@name@_Q(x);
    }

    /** @see gslmm::distribution<Return,Arg>::lower_invcdf */
    virtual return_type lower_invcdf(double p)
    {
      return gsl_cdf_@name@_Pinv(p);
    }
    /** @see gslmm::distribution<Return,Arg>::upper_invcdf */
    virtual return_type upper_invcdf(double q)
    {
      return gsl_cdf_@name@_Qinv(q);
    }
  };
}

    
    
#endif
//____________________________________________________________________
//
// EOF
//
