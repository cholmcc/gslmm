//
// $Id: interpolation.hh,v 1.2 2011-01-24 09:39:38 cholm Exp $ 
//  
//  gslmm::interpolation
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_interploation
#define GSLMM_interploation
#ifndef __GSL_INTERP_H
# include <gsl/gsl_interp.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

/** @file   interpolation/interpolation.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  interploation class */

/** @defgroup interpolation Interpolation

    Given a set of data points @f$ (x_1, y_1) \dots (x_n, y_n)@f$ the 
    routines described in this section compute a continuous
    interpolating function @f$y(x)@f$ such that @f$y(x_i) = y_i@f$.
    The interpolation is piecewise smooth, and its behavior at the 
    end-points is determined by the method of interpolation used.
    
    @todo Implement tests of these classes. */
namespace gslmm {
  // Forward decl. 
  class spline;

  /** 
   * @class interpolation 
   * @ingroup interpolation 
   *
   * Create an interpolation type @a t of the data set @f$ (x_1, y_1)
   * \dots (x_n, y_n)@f$.  Available methods are 
   *
   */
  class interpolation 
  {
  public:
    typedef const std::vector<double> double_array;
    enum method { 
      /**
       * Linear interpolation.  This interpolation method does not
       * require any additional memory.
       */
      linear,
      /** 
       * Polynomial interpolation.  This method should only be used for
       * interpolating small numbers of points because polynomial
       * interpolation introduces large oscillations, even for
       * well-behaved datasets.  The number of terms in the
       * interpolating polynomial is equal to the number of points.
       */
      polynomial,
      /** 
       * Cubic spline with natural boundary conditions.  The resulting
       * curve is piecewise cubic on each interval, with matching first
       * and second derivatives at the supplied data-points.  The second
       * derivative is chosen to be zero at the first point and last
       * point.
       */
      cubic_spline,
      /**
       * Cubic spline with periodic boundary conditions.  The resulting
       * curve is piecewise cubic on each interval, with matching first
       * and second derivatives at the supplied data-points.  The
       * derivatives at the first and last points are also matched.
       * Note that the last point in the data must have the same y-value
       * as the first point, otherwise the resulting periodic
       * interpolation will have a discontinuity at the boundary.
       */
      periodic_cubic_spline,
      /** 
       * Non-rounded Akima spline with natural boundary conditions. This
       * method uses the non-rounded corner algorithm of Wodicka. 
       */
      akima,
      /**
       *  Non-rounded Akima spline with periodic boundary conditions.
       * This method uses the non-rounded corner algorithm of Wodicka.
       */
      periodic_akima
    };
    /**
     * Accelerator for interpolation searches 
     * 
     */
    class accelerator 
    {
    public:
      /** 
       * Constructor.  Allocates a new accelerator for interpolation
       * searches. 
       * 
       */
      accelerator();
      /** 
       * Destructor.  
       * 
       */
      virtual ~accelerator();
      /** 
       * Find the index @a i in array @a of size @a n a such that 
       * @f$ a_{i} \le x \lt a_{i+1}@f$.  The array @a is assumed to
       * be sorted ascending (use for example @c std::sort).  If @f$ x
       * \lt a_{0}@f$ then 0 is returned.  If @f$ x \ge a_{n-1}@f$
       * then @a n-1 is returned. 
       * 
       * @param a Array to search
       * @param n Size of array @a a 
       * @param x Value to search for nearest index. 
       * 
       * @return Nearest index of @a x in array @a a. 
       */
      size_t find(const double* a, size_t n, double x) const;
      /** 
       * Find the index @a i in array @a of size @a n a such that 
       * @f$ a_{i} \le x \lt a_{i+1}@f$.  The array @a is assumed to
       * be sorted ascending (use for example @c std::sort).  If @f$ x
       * \lt a_{0}@f$ then 0 is returned.  If @f$ x \ge a_{n-1}@f$
       * then @a n-1 is returned. 
       * 
       * @param a Array to search
       * @param n Size of array @a a 
       * @param x Value to search for nearest index. 
       * 
       * @return Nearest index of @a x in array @a a. 
       */
      size_t find(double_array& a, double x) const;

      /** 
       * Number of times a find operation missed.
       * 
       * 
       * @return number of misses
       */
      size_t misses() const { return (_low ? _low->miss_count : 0); }
      /** 
       * Number of times a find operation hit.
       * 
       * 
       * @return number of hits
       */
      size_t hits()   const { return (_low ? _low->hit_count  : 0); }
      
      /** Interpolation is a friend */ 
      friend class interpolation;
      /** Spline is a friend */
      friend class spline;
    protected:
      /** Low level entity */
      gsl_interp_accel* _low;
    };
    /** 
     * Allocate an interpolation object of the specified type.
     * 
     * @param t Type of interpolation.
     * @param n Number of data points
     * @param x Array of size @a n of @f$ x@f$ values.  The array is
     * assumed to be sorted in ascending order (use for example @c
     * std::sort). 
     * @param y Array of size @a n of @f$ y@f$ values.
     */
    interpolation(method t, size_t n, const double* x, const double* y);
    /** 
     * Allocate an interpolation object of the specified type.
     * 
     * @param t Method of interpolation.
     * @param x Array of @f$ x@f$ values.  The array is
     * assumed to be sorted in ascending order (use for example @c
     * std::sort). 
     * @param y Array of @f$ y@f$ values.
     */
    interpolation(method t, double_array& x, double_array& y);
    /** 
     * Destructor
     */
    virtual ~interpolation();
    /** 
     * Get the name of the interpolation method
     * 
     * 
     * @return Name of the interpolation method used. 
     */
    const char* name() const;
    /** 
     * Get the minimum number of points needed by this interpolation
     * method. 
     * 
     * 
     * @return Minimum number of points needed by this interpolation.
     */
    size_t points_needed() const;
    
    /** 
     * Low bound of used data.  This defines the lower bound of the
     * domain over which the interpolation is valid.
     * 
     * 
     * @return Lower bound of validity domain
     */    
    double xmin() const { return (_low ? _low->xmin : 0); }
    /** 
     * High bound of used data.  This defines the upper bound of the
     * domain over which the interpolation is valid.
     * 
     * 
     * @return Upper bound of validity domain
     */    
    double xmax() const { return (_low ? _low->xmax : 0); }
    /** 
     * The number of data points used in the interpolation.
     * 
     * 
     * @return Number of data points used. 
     */
    size_t size() const { return (_low ? _low->size : 0); }
    /** 
     * The interpolation method
     * 
     * 
     * @return Interpolation method
     */
    method type() const { return (_low ? low2type(_low->type) : linear); }

    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double operator()(const double* xx, 
		      const double* yy, 
		      double        x) const { return eval(xx,yy,x); }
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double operator()(double_array& xx, 
		      double_array& yy, 
		      double        x) const { return eval(xx,yy,x); }
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double operator()(const double* xx, 
		      const double* yy, 
		      double        x, 
		      accelerator&  a) const  { return eval(xx,yy,x,a); }
    /** 
     * Evaluate the interpolation of @f$ (x,y)@f$ at the point @a x. 
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double operator()(double_array& xx, 
		      double_array& yy, 
		      double        x, 
		      accelerator&  a) const  { return eval(xx,yy,x,a); }
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double eval(const double* xx, 
		const double* yy, 
		double        x) const;
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double eval(double_array& xx, 
		double_array& yy, 
		double        x) const {return eval(&(xx[0]),&(yy[0]),x);}
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value of the interpolation at the point @a x
     */
    double eval(const double* xx, 
		const double* yy, 
		double        x, 
		accelerator&  a) const;
    /** 
     * Evaluate the interpolation of @f$ (x,y)@f$ at the point @a x. 
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double eval(double_array& xx, 
		double_array& yy, 
		double        x, 
		accelerator&  a) const {return eval(&(xx[0]),&(yy[0]),x,a);}

    /** 
     * Evaluate the first derivative @f${dy(x)} \over {dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ {dy(x)} \over {dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv(const double* xx, 
		 const double* yy, 
		 double        x) const;
    /** 
     * Evaluate the first derivative @f${dy(x)} \over {dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ {dy(x)} \over {dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv(double_array& xx, 
		 double_array& yy, 
		 double        x) const {return deriv(&(xx[0]),&(yy[0]),x); }
    /** 
     * Evaluate the first derivative @f${dy(x)} \over {dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ {dy(x)} \over {dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv(const double* xx, 
		 const double* yy, 
		 double        x, 
		 accelerator&  a) const;
    /** 
     * Evaluate the first derivative @f${dy(x)} \over {dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ {dy(x)} \over {dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv(double_array& xx, 
		 double_array& yy, 
		 double        x, 
		 accelerator&  a) const {return deriv(&(xx[0]),&(yy[0]),x,a); }
    /** 
     * Evaluate the first derivative @f${dy(x)} \over {dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ {dy(x)} \over {dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv2(const double* xx, 
		const double* yy, 
		double        x) const;
    /** 
     * Evaluate the second derivative @f${d^2y(x)} \over {dx^2}@f$ of
     * the interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ {d^2y(x)} \over {dx^2}@f$ of the second
     * derivative of the interpolation at the point @a x 
     */
    double deriv2(double_array& xx, 
		  double_array& yy, 
		  double        x) const {return deriv(&(xx[0]),&(yy[0]),x); }
    /** 
     * Evaluate the second derivative @f${d^2y(x)} \over {dx^2}@f$ of
     * the interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ {d^2y(x)} \over {dx^2}@f$ of the second
     * derivative of the interpolation at the point @a x 
     */
    double deriv2(const double* xx, 
		  const double* yy, 
		  double        x, 
		  accelerator&  a) const;
    /** 
     * Evaluate the second derivative @f${d^2y(x)} \over {dx^2}@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data. 
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ {d^2y(x)} \over {dx^2}@f$ of the second
     * derivative of the interpolation at the point @a x 
     */
    double deriv2(double_array& xx, 
		  double_array& yy, 
		  double        x, 
		  accelerator&  a) const{return deriv2(&(xx[0]),&(yy[0]),x,a); }

    /** 
     * Evaluate the definite integral @f$ \int_{a}^{b}dx y(x)@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ over the range @f$
     * [a,b]@f$ 
     * 
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data.   
     * @param a  Lower bound of the integral
     * @param b  Upper bound of the integral
     * 
     * @return The definite integral @f$ \int_{a}^{b}dx y(x)@f$
     */
    double integ(const double* xx, 
		 const double* yy, 
		 double        a,
		 double        b) const;
    /** 
     * Evaluate the definite integral @f$ \int_{a}^{b}dx y(x)@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ over the range @f$
     * [a,b]@f$ 
     * 
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data.   
     * @param a  Lower bound of the integral
     * @param b  Upper bound of the integral
     * 
     * @return The definite integral @f$ \int_{a}^{b}dx y(x)@f$
     */
    double integ(double_array& xx, 
		 double_array& yy, 
		 double        a,
		 double        b) const {return integ(&(xx[0]),&(yy[0]),a,b); }
    /** 
     * Evaluate the definite integral @f$ \int_{a}^{b}dx y(x)@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ over the range @f$
     * [a,b]@f$ 
     * 
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data.   
     * @param a  Lower bound of the integral
     * @param b  Upper bound of the integral
     * @param c  Accelerator used for look-ups.
     * 
     * @return The definite integral @f$ \int_{a}^{b}dx y(x)@f$
     */
    double integ(const double* xx, 
		 const double* yy, 
		 double        a,
		 double        b,
		 accelerator&  c) const;
    /** 
     * Evaluate the definite integral @f$ \int_{a}^{b}dx y(x)@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ over the range @f$
     * [a,b]@f$ 
     * 
     * @param xx Array of the independent variable of the interpolated data. 
     * @param yy Array of the dependent variable of the interpolated data.   
     * @param a  Lower bound of the integral
     * @param b  Upper bound of the integral
     * @param c  Accelerator used for look-ups.
     * 
     * @return The definite integral @f$ \int_{a}^{b}dx y(x)@f$
     */
    double integ(double_array& xx, 
		 double_array& yy, 
		 double        a,
		 double        b,
		 accelerator&  c) const {return integ(&(xx[0]),&(yy[0]),a,b,c);}


  protected: 
    /** Spline is a friend so that we can use the two below static
	member functions. */
    friend class spline;
    /** 
     * find the low-level interpolation method correspondig to our
     * enumeration. 
     * 
     * @param t Enumartion vallue
     * 
     * @return Pointer to interpolation method or null if @a t is
     * invalid. 
     */
    static const gsl_interp_type* type2low(method t);
    /** 
     * Get the enumaration value of a specific interpolation method
     * low-level structure. 
     * 
     * @param t 
     * 
     * @return 
     */
    static method low2type(const gsl_interp_type* t);
    /** The low level interpolation method */
    gsl_interp* _low;
  };

  //__________________________________________________________________
  inline 
  interpolation::accelerator::accelerator()
    : _low(0)
  {
    _low = gsl_interp_accel_alloc();
  }
  //__________________________________________________________________
  inline 
  interpolation::accelerator::~accelerator()
  {
    if (_low) gsl_interp_accel_free(_low);
    _low = 0;
  }
  //__________________________________________________________________
  inline size_t 
  interpolation::accelerator::find(const double* a, size_t n, double x) const
  {
    return gsl_interp_accel_find(_low, a, n, x);
  }
  //__________________________________________________________________
  inline size_t 
  interpolation::accelerator::find(double_array& a, double x) const
  {
    return find(&(a[0]), a.size(), x);
  }
  
  //__________________________________________________________________
  inline 
  interpolation::interpolation(method        t, 
			       size_t        n, 
			       const double* x, 
			       const double* y)
    : _low(0)
  {
    const gsl_interp_type* lt = type2low(t);
    _low = gsl_interp_alloc(lt, n);
    gsl_interp_init(_low, &(x[0]), &(y[0]), n);
  }
  //__________________________________________________________________
  inline 
  interpolation::interpolation(method        t, 
			       double_array& x, 
			       double_array& y)
    : _low(0)
  {
    const gsl_interp_type* lt = type2low(t);
    size_t n = std::min(x.size(), y.size());
    _low = gsl_interp_alloc(lt, n);
    gsl_interp_init(_low, &(x[0]), &(y[0]), n);
  }
  //__________________________________________________________________
  inline
  interpolation::~interpolation()
  {
    if (_low)   gsl_interp_free(_low);
  }
  //__________________________________________________________________
  inline const char* 
  interpolation::name() const
  {
    if (!_low) return 0;
    return gsl_interp_name(_low);
  }
  //__________________________________________________________________
  inline size_t 
  interpolation::points_needed() const
  {
    if (!_low) return 0;
    return gsl_interp_min_size(_low);
  }


  //__________________________________________________________________
  inline double
  interpolation::eval(const double* xx, 
		      const double* yy, 
		      double        x) const
  {
    return gsl_interp_eval(_low, xx, yy, x, 0);
  }
  //__________________________________________________________________
  inline double
  interpolation::eval(const double* xx, 
		      const double* yy, 
		      double        x, 
		      accelerator&  a) const
  {
    return gsl_interp_eval(_low, xx, yy, x, a._low);
  }
  //__________________________________________________________________
  inline double
  interpolation::deriv(const double* xx, 
		      const double* yy, 
		      double        x) const
  {
    return gsl_interp_eval_deriv(_low, xx, yy, x, 0);
  }
  //__________________________________________________________________
  inline double
  interpolation::deriv(const double* xx, 
		      const double* yy, 
		      double        x, 
		      accelerator&  a) const
  {
    return gsl_interp_eval_deriv(_low, xx, yy, x, a._low);
  }
  //__________________________________________________________________
  inline double
  interpolation::deriv2(const double* xx, 
		      const double* yy, 
		      double        x) const
  {
    return gsl_interp_eval_deriv2(_low, xx, yy, x, 0);
  }
  //__________________________________________________________________
  inline double
  interpolation::deriv2(const double* xx, 
		      const double* yy, 
		      double        x, 
		      accelerator&  a) const
  {
    return gsl_interp_eval_deriv2(_low, xx, yy, x, a._low);
  }
  //__________________________________________________________________
  inline double
  interpolation::integ(const double* xx, 
		       const double* yy, 
		       double        a, 
		       double        b) const
  {
    return gsl_interp_eval_integ(_low, xx, yy, a, b, 0);
  }
  //__________________________________________________________________
  inline double
  interpolation::integ(const double* xx, 
		       const double* yy, 
		       double        a, 
		       double        b, 
		       accelerator&  c) const
  {
    return gsl_interp_eval_integ(_low, xx, yy, a, b, c._low);
  }

  //__________________________________________________________________
  inline const gsl_interp_type* 
  interpolation::type2low(method t)  
  {
    const gsl_interp_type* lt = 0;
    switch (t) { 
    case linear:                 lt = gsl_interp_linear;;		break;
    case polynomial:             lt = gsl_interp_polynomial;		break;
    case cubic_spline:           lt = gsl_interp_cspline;		break;
    case periodic_cubic_spline:  lt = gsl_interp_cspline_periodic;	break;
    case akima:                  lt = gsl_interp_akima;			break;
    case periodic_akima:         lt = gsl_interp_akima_periodic;	break;
    }
    return lt;
  }
  //__________________________________________________________________
  inline interpolation::method 
  interpolation::low2type(const gsl_interp_type* t) 
  {
    if      (t == gsl_interp_linear)            return linear;
    else if (t == gsl_interp_polynomial)        return polynomial;
    else if (t == gsl_interp_cspline)           return cubic_spline;
    else if (t == gsl_interp_cspline_periodic)  return periodic_cubic_spline;
    else if (t == gsl_interp_akima)             return akima;
    else if (t == gsl_interp_akima_periodic)    return periodic_akima;
    return linear;
  }
}
#endif
//____________________________________________________________________
//
// EOF
//
