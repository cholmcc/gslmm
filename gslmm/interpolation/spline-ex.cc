#include <iostream>
#include <gslmm/interpolation/spline.hh>
#include <cmath>

/**
 * The following program demonstrates the use of the interpolation and  
 * spline functions.  It computes a cubic spline interpolation of the 
 * 10-point dataset @f$(x_i, y_i)@f$ where @f$ x_i = i + \sin(i)/2@f$ 
 * and @f$ y_i = i + \cos(i^2)@f$ for @f$ i = 0 \dots 9@f$. 
 *
 * The output can be plotted with GNU @i graph.
 *
 * @verbatim
 *
 *   $ ./spline-ex 1 | graph -T x
 * 
 * @endverbatim 
 *
 * The result shows a periodic interpolation of the original
 * points. The slope of the fitted curve is the same at the beginning
 * and end of the data, and the second derivative is also.
 */

using gslmm::interpolation;
using gslmm::spline;

void
ex1()
{
  std::vector<double> x(10);
  std::vector<double> y(10);

  std::cout << "#m=0,S=2" << std::endl;

  for (int i = 0; i < x.size(); i++) { 
    x[i] = i + 0.5 * sin(double(i));
    y[i] = i + 0.5 * cos(double(i * i));
    std::cout << x[i] << " " << y[i] << std::endl;
  }
  
  std::cout << "#m=1,S=0" << std::endl;
  
  interpolation::accelerator a;
  spline      s(interpolation::cubic_spline,x,y);
  
  std::cerr << "Spline interpolation of type " << s.name() << std::endl;

  for (double xi = x[0]; xi < x[x.size()-1]; xi += 0.01) { 
    double yi = s(xi,a);
    std::cout << xi << " " << yi << std::endl;
  }
}

/** 
 * The next program demonstrates a periodic cubic spline with 4 data
 * points.  Note that the first and last points must be supplied with
 * the same y-value for a periodic spline.
 *
 * The output can be plotted with GNU @i graph.
 *
 * @verbatim
 *
 *   $ ./spline-ex 2 | graph -T x
 * 
 * @endverbatim 
 *
 * The result shows a periodic interpolation of the original
 * points. The slope of the fitted curve is the same at the beginning
 * and end of the data, and the second derivative is also.
 */
void 
ex2()
{
  /* Note: first = last for periodic data */
  size_t n   = 4;
  double x[] = { 0.00, 0.10,  0.27,  0.30};
  double y[] = {0.15, 0.70, -0.10,  0.15}; 

  interpolation::accelerator a;
  spline      s(interpolation::periodic_cubic_spline,n,x,y);

  std::cout << "#m=0,S=5" << std::endl;
  for (int i = 0; i < n; i++) 
    std::cout << x[i] << " " << y[i] << std::endl;
  
  std::cout << "#m=1,S=0" << std::endl;
  
  for (int i = 0; i < 100; i++) { 
    double xi = (1 - i / 100.) * x[0] + (i / 100.) * x[n-1];
    double yi = s(xi, a);
    std::cout << xi << " " << yi << std::endl;
  }
}

int
main(int argc, char** argv)
{
  int ex = 1;
  if (argc > 1) { 
    switch (argv[1][0]) { 
    case '1': ex = 1; break;
    case '2': ex = 2; break;
    }
  }
  
  if (ex == 1) ex1();
  else         ex2();

  return 0;
}
//
// EOF
//
