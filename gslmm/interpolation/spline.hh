//
// $Id: spline.hh,v 1.1 2011-01-24 09:39:38 cholm Exp $ 
//  
//  gslmm::spline
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_spline
#define GSLMM_spline
#ifndef __GSL_SPLINE_H
# include <gsl/gsl_spline.h>
#endif
#ifndef GSLMM_interpolation
# include <gslmm/interpolation/interpolation.hh>
#endif

/** @file   interpolation/spline.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  interploation class */

namespace gslmm {
  /** 
   * @class spline 
   * @ingroup interpolation 
   *
   * Create an interpolation type @a t of the data set @f$ (x_1, y_1)
   * \dots (x_n, y_n)@f$.  Available methods are 
   *
   */
  class spline 
  {
  public:
    typedef interpolation::double_array double_array;
    typedef interpolation::method       method;
    typedef interpolation::accelerator  accelerator;
    /** 
     * Allocate an spline object of the specified type.
     * 
     * @param t Type of interpolation.
     * @param n Number of data points
     * @param x Array of size @a n of @f$ x@f$ values.  The array is
     * assumed to be sorted in ascending order (use for example @c
     * std::sort). 
     * @param y Array of size @a n of @f$ y@f$ values.
     */
    spline(method t, size_t n, const double* x, const double* y);
    /** 
     * Allocate an spline object of the specified type.
     * 
     * @param t Method of interpolation.
     * @param x Array of @f$ x@f$ values.  The array is
     * assumed to be sorted in ascending order (use for example @c
     * std::sort). 
     * @param y Array of @f$ y@f$ values.
     */
    spline(method t, double_array& x, double_array& y);
    /** 
     * Destructor
     */
    virtual ~spline();
    /** 
     * Get the name of the interpolation method
     * 
     * 
     * @return Name of the interpolation method used. 
     */
    const char* name() const;
    /** 
     * Get the minimum number of points needed by this interpolation
     * method. 
     * 
     * 
     * @return Minimum number of points needed by this interpolation.
     */
    size_t points_needed() const;
    
    /** 
     * Low bound of used data.  This defines the lower bound of the
     * domain over which the interpolation is valid.
     * 
     * 
     * @return Lower bound of validity domain
     */    
    double xmin() const { return (_low ? _low->interp->xmin: 0); }
    /** 
     * High bound of used data.  This defines the upper bound of the
     * domain over which the interpolation is valid.
     * 
     * 
     * @return Upper bound of validity domain
     */    
    double xmax() const { return (_low ? _low->interp->xmax: 0); }
    /** 
     * The number of data points used in the interpolation.
     * 
     * 
     * @return Number of data points used. 
     */
    size_t size() const { return (_low ? _low->size: 0); }
    /** 
     * The interpolation method
     * 
     * 
     * @return Interpolation method
     */
    method type() const { return (_low ? 
				  interpolation::low2type(_low->interp->type) : 
				  interpolation::linear); }

    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double operator()(double x) const { return eval(x); }
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double operator()(double        x, 
		      accelerator&  a) const  { return eval(x,a); }
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ y(x)@f$ of the interpolation at the point
     * @a x 
     */
    double eval(double x) const;
    /** 
     * Evaluate the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ at the
     * point @a x.  
     *
      * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value of the interpolation at the point @a x
     */
    double eval(double        x, 
		accelerator&  a) const;
    /** 
     * Evaluate the first derivative @f$\frac{dy(x)}{dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ \frac{dy(x)}{dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv(double        x) const;
    /** 
     * Evaluate the first derivative @f$\frac{dy(x)}{dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ \frac{dy(x)}{dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv(double        x, 
		 accelerator&  a) const;
    /** 
     * Evaluate the first derivative @f$\frac{dy(x)}{dx}@f$ of the
     * interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * 
     * @return The value @f$ \frac{dy(x)}{dx}@f$ of the derivative of
     * the interpolation at the point @a x 
     */
    double deriv2(double        x) const;
    /** 
     * Evaluate the second derivative @f$\frac{d^2y(x)}{dx^2}@f$ of
     * the interpolation @f$ y(x)@f$ * of @f$ (x,y)@f$ at the point @a x.  
     *
     * @param x  Where to evaluate the interpolation
     * @param a  Accelerator used for look-ups.
     * 
     * @return The value @f$ \frac{d^2y(x)}{dx^2}@f$ of the second
     * derivative of the interpolation at the point @a x 
     */
    double deriv2(double        x, 
		  accelerator&  a) const;
    /** 
     * Evaluate the definite integral @f$ \int_{a}^{b}dx y(x)@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ over the range @f$
     * [a,b]@f$ 
     * 
     * @param a  Lower bound of the integral
     * @param b  Upper bound of the integral
     * 
     * @return The definite integral @f$ \int_{a}^{b}dx y(x)@f$
     */
    double integ(double        a,
		 double        b) const;
    /** 
     * Evaluate the definite integral @f$ \int_{a}^{b}dx y(x)@f$ of
     * the interpolation @f$ y(x)@f$ of @f$ (x,y)@f$ over the range @f$
     * [a,b]@f$ 
     * 
     * @param a  Lower bound of the integral
     * @param b  Upper bound of the integral
     * @param c  Accelerator used for look-ups.
     * 
     * @return The definite integral @f$ \int_{a}^{b}dx y(x)@f$
     */
    double integ(double        a,
		 double        b,
		 accelerator&  c) const;
  protected: 
    /** The low level interpolation method */
    gsl_spline* _low;
  };

  //__________________________________________________________________
  inline 
  spline::spline(method t, size_t n, 
		 const double* x, 
		 const double* y)
    : _low(0)
  {
    const gsl_interp_type* lt = interpolation::type2low(t);
    if (!lt) return;
    _low = gsl_spline_alloc(lt, n);
    gsl_spline_init(_low, &(x[0]), &(y[0]), n);
  }
  //__________________________________________________________________
  inline 
  spline::spline(method        t, 
		 double_array& x, 
		 double_array& y)
    : _low(0)
  {
    const gsl_interp_type* lt = interpolation::type2low(t);
    if (!lt) return;
    size_t n = std::min(x.size(), y.size());
    _low = gsl_spline_alloc(lt, n);
    gsl_spline_init(_low, &(x[0]), &(y[0]), n);
  }
  //__________________________________________________________________
  inline
  spline::~spline()
  {
    if (_low)   gsl_spline_free(_low);
  }
  //__________________________________________________________________
  inline const char* 
  spline::name() const
  {
    if (!_low) return 0;
    return gsl_spline_name(_low);
  }
  //__________________________________________________________________
  inline size_t 
  spline::points_needed() const
  {
    if (!_low) return 0;
    return gsl_spline_min_size(_low);
  }


  //__________________________________________________________________
  inline double
  spline::eval(double        x) const
  {
    return gsl_spline_eval(_low, x, 0);
  }
  //__________________________________________________________________
  inline double
  spline::eval(double        x, 
	       accelerator&  a) const
  {
    return gsl_spline_eval(_low, x, a._low);
  }
  //__________________________________________________________________
  inline double
  spline::deriv(double        x) const
  {
    return gsl_spline_eval_deriv(_low, x, 0);
  }
  //__________________________________________________________________
  inline double
  spline::deriv(double        x, 
		accelerator&  a) const
  {
    return gsl_spline_eval_deriv(_low, x, a._low);
  }
  //__________________________________________________________________
  inline double
  spline::deriv2(double        x) const
  {
    return gsl_spline_eval_deriv2(_low, x, 0);
  }
  //__________________________________________________________________
  inline double
  spline::deriv2(double        x, 
		 accelerator&  a) const
  {
    return gsl_spline_eval_deriv2(_low, x, a._low);
  }
  //__________________________________________________________________
  inline double
  spline::integ(double        a, 
		double        b) const
  {
    return gsl_spline_eval_integ(_low, a, b, 0);
  }
  //__________________________________________________________________
  inline double
  spline::integ(double        a, 
		double        b, 
		accelerator&  c) const
  {
    return gsl_spline_eval_integ(_low, a, b, c._low);
  }
}
#endif
//____________________________________________________________________
//
// EOF
//
