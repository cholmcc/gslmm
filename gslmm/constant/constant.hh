//
// $Id: constant.hh,v 1.2 2003-03-12 13:52:05 cholm Exp $ 
//  
//  gslmm::constant
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_constant
#define GSLMM_constant

/** @file   constant/constant.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  constant class */

/** @defgroup constant Physical Constants
    @todo Implement these classes. These should be implemented as
    templated structures with static data members. */

#endif
//____________________________________________________________________
//
// EOF
//
