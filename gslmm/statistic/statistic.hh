//
// $Id: statistic.hh,v 1.4 2006-02-08 00:08:50 cholm Exp $ 
//  
//  gslmm::statistic
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef GSLMM_statistic
#define GSLMM_statistic

/** @file   statistic/statistic.hh
    @author Christian Holm
    @date   Wed Mar 12 14:11:20 2003
    @brief  statistic class */

namespace gslmm 
{
  
  /** @defgroup statistic Statistics */
}

#ifndef GSLMM_statistic_dataset_double
# include <gslmm/statistic/dataset_double.hh>
#endif
#ifndef GSLMM_statistic_dataset_float
# include <gslmm/statistic/dataset_float.hh>
#endif
#ifndef GSLMM_statistic_dataset_long_double
# include <gslmm/statistic/dataset_long_double.hh>
#endif
#ifndef GSLMM_statistic_dataset_unsigned_long
# include <gslmm/statistic/dataset_unsigned_long.hh>
#endif
#ifndef GSLMM_statistic_dataset_long
# include <gslmm/statistic/dataset_long.hh>
#endif
#ifndef GSLMM_statistic_dataset_unsigned_int
# include <gslmm/statistic/dataset_unsigned_int.hh>
#endif
#ifndef GSLMM_statistic_dataset_int
# include <gslmm/statistic/dataset_int.hh>
#endif
#ifndef GSLMM_statistic_dataset_unsigned_short
# include <gslmm/statistic/dataset_unsigned_short.hh>
#endif
#ifndef GSLMM_statistic_dataset_short
# include <gslmm/statistic/dataset_short.hh>
#endif
#ifndef GSLMM_statistic_dataset_unsigned_char
# include <gslmm/statistic/dataset_unsigned_char.hh>
#endif
#ifndef GSLMM_statistic_dataset_char
# include <gslmm/statistic/dataset_char.hh>
#endif
#ifndef GSLMM_statistic_weighted_dataset_double
# include <gslmm/statistic/weighted_dataset_double.hh>
#endif
#ifndef GSLMM_statistic_weighted_dataset_float
# include <gslmm/statistic/weighted_dataset_float.hh>
#endif
#ifndef GSLMM_statistic_weighted_dataset_long_double
# include <gslmm/statistic/weighted_dataset_long_double.hh>
#endif


#endif
//____________________________________________________________________
//
// EOF
//
